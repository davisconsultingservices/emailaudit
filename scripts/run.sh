
# Check if email is provided
if [ -z "$1" ]; then
    echo "Usage: $0 <email> <username:password>"
    exit 1
fi
# Check if dehashed credentials are provided
if [ -z "$2" ]; then
    echo "Usage: $0 <domain> <username:password>"
    exit 1
fi
./compromised_creds.sh $1 $2
./create_csv.sh $1
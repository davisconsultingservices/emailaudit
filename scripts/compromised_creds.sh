#exposed credentials via dehashed

rm compromised_credentials.json 2> /dev/null

touch compromised_credentials.json


curl "https://api.dehashed.com/search?query=email:$1&size=10000" \
-u $2 \
-H 'Accept: application/json' > compromised_credentials.json

#!/bin/bash

rm compromised_credentials.csv 2> /dev/null

# Process compromised credential Data
entries=$(cat compromised_credentials.json | jq -c '.entries')
if [ "$entries" != "null" ]; then
    echo "Compromised credentials found."
    cat compromised_credentials.json  | jq -c '.entries[]' | jq -s | jq -r '(.[0] | keys_unsorted) as $keys | $keys, map([.[ $keys[] ]])[] | @csv' > compromised_credentials.csv
else
    echo "No compromised credentials found."
fi

cat compromised_credentials.csv
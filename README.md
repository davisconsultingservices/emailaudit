# emailaudit - Check for domains where specified email credentials have been exposed

This docker container looks for details about exposed credentials for a given email address. Breached Credential scanning depends on valid credentials for dehashed.com

Please report vulnerabilities in emailaudit directly to <security@davisconsulting.services>.

## Dependencies
* [Docker](https://www.docker.com/) available on the terminal prompt
* [Dehashed](https://dehashed.com/) breached credentials provided

## Installation
```
git clone git@gitlab.com:davisconsultingservices/emailaudit.git
```

## Building the Docker container
```
docker build -t emailaudit .
```

## Running the Docker container with dehashed credentials
```
docker run -it emailaudit name@example.com username:password
```

FROM debian:bookworm-slim

# Update and upgrade dependencies
RUN apt update
RUN apt upgrade -y
RUN apt install curl -y
RUN apt install jq -y 
RUN apt autoremove

# Create and set the working directory for scripts
RUN mkdir /scripts
WORKDIR /scripts

# Copy the existing scripts into the container
COPY ./scripts/compromised_creds.sh .
COPY ./scripts/create_csv.sh .
COPY ./scripts/run.sh .

# Entry point for the container
ENTRYPOINT ["/bin/bash", "/scripts/run.sh"]
